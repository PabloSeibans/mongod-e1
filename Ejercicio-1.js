/*  a) CREACION DE LA COLECCIONES    */

db.getCollection("usuario").insertMany([
                                            {
                                                "Nombres": "Pablo",
                                                "Apellidos": "Fernandez Aduviri",
                                                "Correo": "pablo.juegos12345@gmail.com",
                                                "Contrasena": "545sdsd5",
                                                "Direccion": "Av: San Martin #413",
                                                "tarjeta_debito": "2548 2548 5978 2147",
                                                "fecha_registro": new Date() 
                                             },
                                             {
                                                 "Nombres": "Alfredo",
                                                 "Apellidos": "Nogales Garcia",
                                                 "Correo": "alfredo.nogales@hotmail.com",
                                                 "Contrasena": "45781545",
                                                 "Direccion": "Av. Heroinas #12",
                                                 "tarjeta_debito": "2545 2455 8745 3245",
                                                 "fecha_registro": new Date()
                                             },
                                             {
                                                 "Nombres": "Malenna",
                                                 "Apellidos": "Torrez Mendez",
                                                 "Correo": "mtorrez@gmail.com",
                                                 "Contrasena": "74184sx",
                                                 "Direccion": "Av. Los Olmos #48",
                                                 "tarjeta_debito": "4875 2598 2351 2159",
                                                 "fecha_registro": new Date()
                                              },
                                             {
                                                 "Nombres": "Rosa",
                                                 "Apellidos": "Valencia Del Prado",
                                                 "Correo": "rvalencia@gmail.com",
                                                 "Contrasena": "rosavalencia123",
                                                 "Direccion": "Av. Los Olmos #79",
                                                 "tarjeta_debito": "4875 2658 1235 1597",
                                                 "fecha_registro": new Date() 
                                             },
                                             {
                                                 "Nombres": "Juan Pablo",
                                                 "Apellidos": "Lopez Guevara",
                                                 "Correo": "jplopez@gmail.com",
                                                 "Contrasena": "hola123",
                                                 "Direccion": "Av. San Esteban #487",
                                                 "tarjeta_debito": "8745 3526 9548 1654",
                                                 "fecha_registro": new Date() 
                                             },
                                       ])
                                             
db.getCollection("usuario").find({"Apellidos":"Fernandez Aduviri"})

var idPablo = ObjectId("62b25935724ef78f78622252")
var idAlfredo = ObjectId("62b25935724ef78f78622253")
var idMalena = ObjectId("62b25935724ef78f78622254")
var idRosa = ObjectId("62b25935724ef78f78622255")
var idLopez = ObjectId("62b25935724ef78f78622256")
db.getCollection("registrado").insertMany([
                                             {
                                               "Nombres": "Pablo",
                                               "Apellidos": "Fernandez Aduviri",
                                               "Correo": "pablo.juegos12345@gmail.com",
                                               "estadoCuenta":"activo",
                                               "fecha_registro": new Date(),
                                               "idUsuario": idPablo
                                             },
                                             {
                                                "Nombres": "Alfredo",
                                                "Apellidos": "Nogales Garcia",
                                                "Correo": "alfredo.nogales@hotmail.com",
                                                "estadoCuenta":"activo",
                                                "fecha_registro": new Date(),
                                                "idUsuario": idAlfredo
                                              },
                                              {
                                                "Nombres": "Malenna",
                                                "Apellidos": "Torrez Mendez",
                                                "Correo": "mtorrez@gmail.com",
                                                "estadoCuenta":"inactivo",
                                                "fecha_registro": new Date(),
                                                "idUsuario": idMalena
                                              },
                                              {
                                                "Nombres": "Rosa",
                                                "Apellidos": "Valencia Del Prado",
                                                "Correo": "rvalencia@gmail.com",
                                                "estadoCuenta":"activo",
                                                "fecha_registro": new Date(),
                                                "idUsuario": idRosa
                                              },
                                              {
                                                "Nombres": "Juan Pablo",
                                                "Apellidos": "Lopez Guevara",
                                                "Correo": "jplopez@gmail.com",
                                                "estadoCuenta":"activo",
                                                "fecha_registro": new Date(),
                                                "idUsuario": idLopez
                                              }
                                         ])
var idPablo = ObjectId("62b25935724ef78f78622252")
var idAlfredo = ObjectId("62b25935724ef78f78622253")
var idMalena = ObjectId("62b25935724ef78f78622254")
var idRosa = ObjectId("62b25935724ef78f78622255")
var idLopez = ObjectId("62b25935724ef78f78622256")
db.getCollection("articulo").insertMany([
                                             {
                                               "Nombres": ["Producto1", "Producto2"],
                                               "Cantidad": 2,
                                               "Monto": 54.80,
                                               "fecha_compra": new Date(),
                                               "idUsuario": idPablo
                                             },
                                             {
                                                "Nombres": ["Producto1", "Producto5", "Producto4"],
                                                "Cantidad": 3,
                                                "Monto": 85.70,
                                                "fecha_compra": new Date(),
                                                "idUsuario": idAlfredo
                                              },
                                              {
                                                "Nombres": ["Producto3"],
                                                "Cantidad": 1,
                                                "Monto": 25.50,
                                                "fecha_compra": new Date(),
                                                "idUsuario": idMalena
                                              },
                                              {
                                                "Nombres": ["Producto7", "Producto5"],
                                                "Cantidad": 2,
                                                "Monto": 68.0,
                                                "fecha_compra": new Date(),
                                                "idUsuario": idRosa
                                              },
                                              {
                                                "Nombres": ["Producto2", "Producto6", "Producto4"],
                                                "Cantidad": 3,
                                                "Monto": 135.30,
                                                "fecha_compra": new Date(),
                                                "idUsuario": idLopez
                                              }
                                         ])
                                              
/*    b)    CREACION DE LOS INDICES DE BUSQUEDA OPTIMIZADA  :V     */
db.getCollection("usuario").createIndex({"Apellidos": 1})
db.getCollection("usuario").createIndex({"tarjeta_debito": 1})

db.getCollection("usuario").getIndexes()


db.getCollection("registrado").createIndex({"Apellidos": 1})
db.getCollection("registrado").createIndex({"estadoCuenta": 1})

db.getCollection("registrado").getIndexes()

db.getCollection("articulo").createIndex({"fecha_compra": 1})

db.getCollection("articulo").getIndexes()



/*  c)   CONSULTAS PARA CADA COLECCION    */

db.getCollection('usuario').find({})
db.getCollection('registrado').find({})
db.getCollection('articulo').find({})

/*  e) LOS CORREOS ELECTRONICOS NO PUEDEN REPETIRSE  */
db.getCollection("usuario").createIndex({"Correo": 1},{unique: true})
db.getCollection("registrado").createIndex({"Correo": 1},{unique: true})

db.getCollection("usuario").getIndexes()
db.getCollection("registrado").getIndexes()

/*   d)   VERIFICANDO QUE UN USUARIO REALIZO UNA COMPRA  */
db.getCollection("articulo").find({})
db.getCollection("usuario").find({})

var idPablo = ObjectId("62b25935724ef78f78622252")
db.getCollection("articulo").find({"idUsuario": idPablo})
